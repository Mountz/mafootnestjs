## NOTE

* This Project wont work due to lack of environment config
* This Project is for presenting in resume of Tharit Horpetrungraung only

## Technology in this Project

* Clean Architecture
* Backend
  - GraphQL
  - NestJS
  - TypeORM
* Database
  - PostgreSQL  
  - Redis  
