import { Module } from '@nestjs/common';
import { QuizUsecaseV1 } from './usecases/quiz.usecase.impl.v1';

export const IQuizUsecaseInjectionToken = 'Application_IQuizUsecase';
export const IQuizDaoInjectionToken = 'Application_IQuizDao';
export const ISessionUsecaseInjectionToken = 'Application_ISessionUsecase';
export const ISessionDaoInjectionToken = 'Application_ISessionDao';
export const IQuestionDaoInjectionToken = 'Application_IQuestionDao';
export const IChoiceDaoInjectionToken = 'Application_IChoiceDao';
export const IEventDaoInjectionToken = 'Applicaition_IEventDao';
export const IEventUsecaseInjectionToken = 'Application_IEventUsecase';
export const IEventPubsubInjectionToken = 'Application_IEventPubsub';
export const IKeyValueDatabaseInjectionToken = 'Application_IKeyValueDatabase'
export const IScoreCalculatorInjectionToken = 'Application_IScoreCalculator'

@Module({
  providers: [],
  exports: []
})
export class AppApplicationModule {}