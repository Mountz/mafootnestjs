import { setTimeout, clearTimeout } from "timers";

import { Inject } from "@nestjs/common";

import { ISessionDaoInjectionToken, IEventDaoInjectionToken, IQuizDaoInjectionToken, IQuestionDaoInjectionToken, IScoreCalculatorInjectionToken } from "../application.module";
import { Quiz, Score, Session, Player } from "@/domain/entities";

import { ISessionUsecase } from "../interfaces/usecase/session.usecase.interface";
import { ISessionDao } from "../interfaces/dao/session.dao.interface";
import { IRandomGenerator } from "../interfaces/random.generator.interface";
import { IEventDao } from "../interfaces/dao/event.dao.interface";
import { IDateTime } from "../interfaces/datetime.interface";
import { IScoreCalculator } from "../interfaces/score.calculator.interface";

import { IRandomGeneratorInjectionToken, IDateTimeInjectionToken } from "@/infrastructure/infrastructure.module";

export class SessionUsecaseV1 implements ISessionUsecase {
  private readonly sessionTimeoutMap: Map<number,NodeJS.Timer>;
  constructor(
    @Inject(ISessionDaoInjectionToken) private readonly sessionDao : ISessionDao,
    @Inject(IEventDaoInjectionToken) private readonly eventDao : IEventDao,
    @Inject(IRandomGeneratorInjectionToken) private readonly randomGenerator : IRandomGenerator,
    @Inject(IDateTimeInjectionToken) private readonly dateTime: IDateTime,
    @Inject(IScoreCalculatorInjectionToken) private readonly scoreCal: IScoreCalculator,
    ) { 
      this.sessionTimeoutMap = new Map();
    }

  async getById(id: number): Promise<Session> {
    const result = this.sessionDao.getById(id);
    return result;
  }

  async getByCode(code: string): Promise<Session> {
    const result = this.sessionDao.getByCode(code);
    return result;
  }

  async checkByCode(code: string): Promise<boolean> {
    const result = await this.sessionDao.getByCode(code);
    return result ? true: false;
  }

  async getIdByCode(code: string): Promise<number> {
    const result = await this.sessionDao.getIdByCode(code);
    return result;
  }

  async getPlayers(id: number, size: number): Promise<Player[]> {
    const result = this.sessionDao.getPlayers(id, size);
    return result;
  }

  async getScores(id: number, size: number): Promise<Score[]> {
    const result = this.sessionDao.getScores(id, size);
    return result;
  }
  async createSession(quizId: number, title: string): Promise<number> {
    // const code = this.randomGenerator.getRandomNumber(100000, 999999).toString();
    const code = this.randomGenerator.getRandomString(6, true);
    const result = await this.sessionDao.createSession(quizId, code, title);
    const timer = setTimeout(() => {
      //console.log("session %s timeout", code);
      this.deleteSessionById(result);
    }, 7200000);
    if (result) {
      return result;
    }
    return null;
  }
  async joinSession(code: string, name: string): Promise<Player> {
    const player : Player = {
      id: undefined,
      displayName: name,
      token: this.randomGenerator.getRandomString(10, false),
    }
    const session = await this.sessionDao.getByCode(code);
    if (!session) {
      return null;
    }
    const result = await this.sessionDao.addPlayerById(session.id, player);
    if (!result) {
      return null;
    }
    const r = await this.eventDao.addPlayerEvent(session.id,result.displayName);
    if (!r) {
      return null;
    }
    return result;
  }
  async quitSession(token: string): Promise<boolean> {
    const player = await this.sessionDao.getPlayerByToken(token);
    const result = await this.sessionDao.deletePlayerById(player.id);
    return result;
  }
  async endQuestion(code: string): Promise<number> {
    const session = await this.sessionDao.getByCode(code);
    const result = await this.eventDao.endQuestion(session.id);
    clearTimeout(this.sessionTimeoutMap.get(session.id));
    //console.log("cancel %s timeout", code);
    return 1;
  }
  async startQuestion(code: string, index: number): Promise<number> {
    const session = await this.sessionDao.getByCode(code);
    const result = await this.eventDao.startQuestion(session.id, index);
    const timer = setTimeout(() => {
      //console.log("session %s timeout", code);
      this.endQuestion(code);
    }, 10000);
    this.sessionTimeoutMap.set(session.id,timer);
    return 1;
  }

  async deleteSessionById(id: number): Promise<boolean> {
    const result = await this.sessionDao.deleteSessionById(id);
    return result;
  }

  async submitAnswer(token: string, answer: number): Promise<number> {
    const time = this.dateTime.getUnixMilliSeconds();
    const player = await this.sessionDao.getPlayerByToken(token);
    const sessionId = await this.sessionDao.getIdByPlayerToken(token)
    const session = await this.sessionDao.getById(sessionId);
    const latestQuestionEvent = await this.eventDao.getLatestQuestionEvent(sessionId);
    const interval = (time / 1000) - latestQuestionEvent.date;
    const data = JSON.parse(latestQuestionEvent.data);
    const question = session.quiz.questions.find(x => x.index == data.questionIndex);
    let score = 0;
    if (question.correctChoices.includes(answer)) {
      // calculate score 
      score = this.scoreCal.calculateScore(1000, interval, 10);
      // update score
      this.sessionDao.increasePlayerScore(player.id, score);
    }
    return score;
  }
}