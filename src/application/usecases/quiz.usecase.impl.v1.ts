import { Injectable, Inject, forwardRef } from "@nestjs/common";
import { Quiz } from "@/domain/entities/quiz.entity";
import { IQuizUsecase } from "../interfaces/usecase/quiz.usecase.interface";
import { IQuizDao, QuestionUpdateDTO } from "../interfaces/dao/quiz.dao.interface";
import { IQuizDaoInjectionToken, IChoiceDaoInjectionToken, IQuestionDaoInjectionToken } from "../application.module";
import { Question } from "@/domain/entities";
import { IChoiceDao } from "../interfaces/dao/choice.dao.interface";
import { IQuestionDao } from "../interfaces/dao/question.dao.interface";

@Injectable()
export class QuizUsecaseV1 implements IQuizUsecase {
  constructor(
    @Inject(IQuizDaoInjectionToken)
    private readonly quizDao: IQuizDao,
    @Inject(IChoiceDaoInjectionToken)
    private readonly choiceDao: IChoiceDao,
    @Inject(IQuestionDaoInjectionToken)
    private readonly questionDao: IQuestionDao,
  ) { }

  async getById(id: number): Promise<Quiz> {
    const result = await this.quizDao.getById(id);
    return result;
  }
  async getMany(size: number): Promise<Quiz[]> {
    const result = await this.quizDao.getMany(size);
    if(result) {
      return result;
    }
    return [];
  }
  async updateQuizTitleById(id: number, title: string): Promise<boolean> {
    const result = await this.quizDao.updateQuizTitleById(id, title);
    return result;
  }
  async addQuestionById(id: number, questions: Question[]): Promise<number> {
    const result = await this.quizDao.addQuestionById(id, questions);
    return result;
  }
  async updateQuestionById(id: number, questions: QuestionUpdateDTO[]): Promise<boolean> {
    const quiz = await this.quizDao.getById(id);
    if (!quiz) return false;
    for (let question of questions) {
      const updated = await this.questionDao.updateByQuizId(id, question);
      if(!updated) return false;
      let i = quiz.questions.findIndex(x => x.index == question.index);
      if (i < 0) {
        quiz.questions = await this.questionDao.getByQuizId(id);
        i = quiz.questions.findIndex(x => x.index == question.index);
      }
      if (!question.correctChoices) {
        question.correctChoices = quiz.questions[i].correctChoices;
      }
      if (question.choices) {
        const r = await this.choiceDao.updateChoiceByQuestionId(quiz.questions[i].id, question.choices, question.correctChoices)
        if (r <= 0) {
          return false;
        }
      }
    }
    return true;
  }
  async deleteQuestionByIndex(id: number, index: number): Promise<boolean> {
    const quiz = await this.quizDao.getById(id);
    const question = quiz.questions.find(x => x.index == index);
    const result = await this.questionDao.removeById(question.id);
    return result > 0;
  }
  async deleteQuizById(id: number): Promise<boolean> {
    const result = await this.quizDao.deleteQuizById(id);
    return result;
  }

  async createQuiz(quiz: Quiz): Promise<void> {
    const result = await this.quizDao.createQuiz(quiz);
    return;
  }
}