import { Event } from "@/domain/entities";
import { Inject } from "@nestjs/common";
import { ISessionDaoInjectionToken, IEventDaoInjectionToken, IEventPubsubInjectionToken } from "../application.module";
import { IEventDao } from "../interfaces/dao/event.dao.interface";
import { IEventUsecase } from "../interfaces/usecase/event.usecase.interface";
import { IEventPubSub } from "../interfaces/pubsub/event.pubsub.interface";

export class EventUsecaseV1 implements IEventUsecase {

  constructor(
    @Inject(IEventDaoInjectionToken) private readonly eventDao : IEventDao,
    @Inject(IEventPubsubInjectionToken) private readonly eventPubsub: IEventPubSub,
  ) { }
  async getBySessionId(sessionId: number, from: number, to: number): Promise<Event[]> {
    if (to < from) {
      return [];
    }
    const eventList = await this.eventDao.getBySessionId(sessionId);
    const unseen = await eventList.filter(x => {
      return x.sequence >= from && x.sequence <= to;
    });
    return unseen;
  }
    
}