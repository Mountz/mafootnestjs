import { Quiz } from "@/domain/entities/quiz.entity";
import { Question } from "@/domain/entities";
import { QuestionUpdateDTO } from "../dao/quiz.dao.interface";

export interface IQuizUsecase {
  getById(id: number): Promise<Quiz>;
  getMany(size: number): Promise<Quiz[]>;
  createQuiz(quiz: Quiz): Promise<void>
  updateQuizTitleById(id: number, title: string): Promise<boolean>
  addQuestionById(id: number, questions: Question[]): Promise<number>
  updateQuestionById(id: number, questions: QuestionUpdateDTO[]): Promise<boolean>
  deleteQuestionByIndex(id: number, index: number): Promise<boolean>;
  deleteQuizById(id: number): Promise<boolean>;
}