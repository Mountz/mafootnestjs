import { Score, Session, Player } from "@/domain/entities";

export interface ISessionUsecase {
  getById(id: number): Promise<Session>;
  getByCode(code: string): Promise<Session>;
  checkByCode(code: string): Promise<boolean>;
  getIdByCode(code: string): Promise<number>;
  getPlayers(id: number, size: number): Promise<Player[]>;
  getScores(id: number, size: number): Promise<Score[]>;
  createSession(quizId: number, title: string): Promise<number>;
  joinSession(code: string, name: string): Promise<Player>;
  quitSession(token: string): Promise<boolean>;
  endQuestion(code: string): Promise<number>;
  startQuestion(code: string, index: number): Promise<number>;
  deleteSessionById(id: number): Promise<boolean>;
  submitAnswer(token: string, answer: number): Promise<number>;
}