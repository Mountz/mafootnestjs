import { Event } from "@/domain/entities";

export interface IEventUsecase {
  getBySessionId(sessionId: number, from: number, to: number): Promise<Event[]>;
}