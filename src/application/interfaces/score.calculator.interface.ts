export interface IScoreCalculator {
  calculateScore(maxScore: number, interval: number, maxTime: number): number;
}