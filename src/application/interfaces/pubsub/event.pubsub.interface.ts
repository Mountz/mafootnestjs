import { Event } from "@/domain/entities";

export interface IEventPublisher {
  publish(sessionId: number, event: Event[]): Promise<number>
}

export interface IEventSubscription {
  subscription(sessionId: number): Promise<AsyncIterator<Event[]>>;
}

export interface IEventPubSub extends IEventPublisher, IEventSubscription {
}