export interface IDateTime {
    getUnixSeconds(): number;
    getUnixMilliSeconds(): number;
}