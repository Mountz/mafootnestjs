export interface IUnitOfWork {
    commit(): Promise<number>;
}