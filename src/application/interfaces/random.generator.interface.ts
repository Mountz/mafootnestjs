export interface IRandomGenerator {
  getRandomString(lenght: number, allowNumber: boolean): string;
  getRandomNumber(min: number, mas: number): number;
}