import { IDao } from "./dao.interface";
import { Choice } from "@/domain/entities";
import { QuestionUpdateDTO } from "./quiz.dao.interface";
import { Question } from "@/graphql/types/question.type";


export interface IQuestionDaoRead {
  getByQuizId(id: number): Promise<Question[]>;
  getByIndex(quizId: number, index: number): Promise<Question>
}

export interface IQuestionDaoWrite {
  removeById(id: number): Promise<number>;
  updateByQuizId(id: number, question: QuestionUpdateDTO): Promise<boolean>;
}

export interface IQuestionDao extends IQuestionDaoRead, IQuestionDaoWrite, IDao {
}