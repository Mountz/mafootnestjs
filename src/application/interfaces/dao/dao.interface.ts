export interface IDao {
    saveChanges(): Promise<number>
}