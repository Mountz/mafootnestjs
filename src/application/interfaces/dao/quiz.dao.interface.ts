import { Question } from "@/domain/entities/question.entity";
import { Quiz } from "@/domain/entities/quiz.entity";
import { IDao } from "./dao.interface";

export interface IQuizDaoRead {
  getById(id: number): Promise<Quiz>;
  getMany(size: number): Promise<Quiz[]>;
}

export interface IQuizDaoWrite {
  createQuiz(quiz: Quiz): Promise<void>;
  updateQuizTitleById(id: number, title: string): Promise<boolean>;
  addQuestionById(id: number, questions: Question[]): Promise<number>;
  updateQuestionById(id:number, question: QuestionUpdateDTO): Promise<boolean>;
  deleteQuizById(id: number): Promise<boolean>;
}

export interface IQuizDao extends IQuizDaoRead, IQuizDaoWrite, IDao {
}

export interface QuestionUpdateDTO {
  index: number;
  title?: string;
  choices?: ChoiceUpdateDTO[];
  correctChoices?: number[];
}

export interface ChoiceUpdateDTO {
  index: number;
  content?: string;
}