import { IDao } from "./dao.interface";
import { Choice } from "@/domain/entities";
import { ChoiceUpdateDTO } from "./quiz.dao.interface";


export interface IChoiceDaoRead {
  getByQuestionId(id: number): Promise<Choice[]>;
}

export interface IChoiceDaoWrite {
  removeChoiceByQuestionId(id: number): Promise<number>;
  updateChoiceByQuestionId(id: number, choices: ChoiceUpdateDTO[], correctList: number[]): Promise<number>;
}

export interface IChoiceDao extends IChoiceDaoRead, IChoiceDaoWrite, IDao {
}