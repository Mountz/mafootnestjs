import { IDao } from "./dao.interface";
import { Choice } from "@/domain/entities";
import { Event } from "@/graphql/types/event.type";


export interface IEventDaoRead {
  getBySessionId(sessionId: number): Promise<Event[]>;
  getLatestQuestionEvent(sessionId: number): Promise<Event>;
}

export interface IEventDaoWrite {
  addBySessionId(sessionId: number, event: Event): Promise<number>
  addPlayerEvent(sessionId: number, playerName: string): Promise<number>
  startQuestion(sessionId: number, index: number): Promise<number>;
  endQuestion(sessionId: number): Promise<number>;
}

export interface IEventDao extends IEventDaoRead, IEventDaoWrite, IDao {
}