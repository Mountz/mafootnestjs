import { Player } from "@/domain/entities/player.entity";
import { Session } from "@/domain/entities/session.entity";
import { IDao } from "./dao.interface";
import { Score } from "@/domain/entities/score.entity";

export interface ISessionDaoRead {
  getById(id: number): Promise<Session>;
  getByCode(code: string): Promise<Session>;
  getIdByCode(code: string): Promise<number>;
  getScores(id: number, size: number): Promise<Score[]>;
  getPlayers(id: number, size: number): Promise<Player[]>;
  getPlayerByToken(token: string): Promise<Player>;
  getIdByPlayerToken(token: string): Promise<number> 
}

export interface ISessionDaoWrite {
  createSession(quizId: number, code: string, title: string): Promise<number>;
  addPlayerById(id: number, player: Player): Promise<Player>;
  deletePlayerById(playerId: number): Promise<boolean>;
  deleteSessionById(id: number): Promise<boolean>;
  increasePlayerScore(id: number, score: number): Promise<boolean>;
}

export interface ISessionDao extends ISessionDaoRead, ISessionDaoWrite, IDao {
}