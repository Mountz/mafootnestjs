export interface IKeyValueDatabase {
  get(key: string): Promise<string>;
  set(key: string, value: string): Promise<boolean>;
  increment(key: string): Promise<number>;
  expire(key: string, seconds: number): Promise<number>;
}