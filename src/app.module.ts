import path from 'path';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RedisModule } from 'nestjs-redis'

import { AppGraphQLModule } from '@graphql/graphql.module';

import { Quiz, Choice, Question, Player, Score, Session, Event} from '@persistence/models';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: [
        Quiz,
        Question,
        Choice,
        Player,
        Score,
        Session,
        Event,
      ],
      synchronize: true,
      logging: process.env.NODE_ENV === "production" ? [] : ["query"],
    }),
    AppGraphQLModule,
    GraphQLModule.forRoot({
      path: '/api/gql',
      subscriptions: {
        path: '/ws/gql'
      },
      debug: process.env.NODE_ENV === "production" ? false : true,
      playground: process.env.NODE_ENV === "production" ? false : true,
      include: [ AppGraphQLModule ],
      installSubscriptionHandlers: true,
      autoSchemaFile: path.join(process.cwd(), 'src/graphql/schema.generated.graphql')
    }),
    RedisModule.register({
      host: process.env.REDIS_HOST,
      port: +process.env.REDIS_PORT,
      password: process.env.REDIS_AUTH,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
