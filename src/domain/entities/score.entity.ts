export interface Score {
  id: number;
  playerId: number;
  score: number;
}