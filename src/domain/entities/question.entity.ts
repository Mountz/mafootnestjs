import { Choice } from "./choice.entity";

export interface Question {
  id: number;
  index: number;
  title: string;
  choices: Choice[];
  correctChoices: number[];
}