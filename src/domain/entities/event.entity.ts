export interface Event {
  id: number,
  sequence: number,
  type: "PlayerJoined" | "QuestionStarted" | "QuestionEnded",
  data: string,
  date: number,
}