import { Question } from "./question.entity";

export interface Quiz {
    id: number;
    title: string;
    questions: Question[];
}