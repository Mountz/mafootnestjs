export interface Choice {
  id: number;
  index: number;
  content: string;
}