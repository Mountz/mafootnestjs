import { Player } from "./player.entity";
import { Quiz } from "./quiz.entity";
import { Score } from "./score.entity";


export interface Session {
  id: number;
  quiz: Quiz;
  code: string;
  title: string;
  players: Player[];
  scores: Score[];
}