export interface Player {
  id: number;
  displayName: string;
  token: string;
}