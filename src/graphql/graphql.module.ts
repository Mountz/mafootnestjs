import { Module, forwardRef } from '@nestjs/common';
import { QuestionResolver } from './resolvers/question.resolver';
import { QuizResolver } from './resolvers/quiz.resolver';
import { QuizMutationResolver } from './resolvers/quiz.mutation.resolver';
import { SessionResolver } from './resolvers/session.resolver';
import { SessionMutationResolver } from './resolvers/session.mutation.resolver';
import { SessionSubscriptionResolver } from './resolvers/session.subscription.resolver';


import { IQuizDaoInjectionToken, IQuizUsecaseInjectionToken, ISessionDaoInjectionToken, ISessionUsecaseInjectionToken, IChoiceDaoInjectionToken, IEventDaoInjectionToken, IEventPubsubInjectionToken, IQuestionDaoInjectionToken, IEventUsecaseInjectionToken, IKeyValueDatabaseInjectionToken, IScoreCalculatorInjectionToken } from '@/application/application.module';
import { QuizUsecaseV1 } from '@/application/usecases/quiz.usecase.impl.v1';
import { SessionUsecaseV1 } from '@/application/usecases/session.usecase.impl.v1';
import { EventUsecaseV1 } from '@/application/usecases/event.usecase.impl.v1';

import { QuizDaoV1 } from '@/persistence/dao/quiz.dao.impl.v1';
import { SessionDaoV1 } from '@/persistence/dao/session.dao.impl.v1';
import { ChoiceDaoV1 } from '@/persistence/dao/choice.dao.impl.v1';
import { QuestionDaoV1 } from '@/persistence/dao/question.dao.impl.v1';
import { EventDaoV1 } from '@/persistence/dao/event.dao.impl.v1';
import { EventPubsubV1 } from '@/persistence/pubsub/event.pubsub.impl.v1';
import { RedisClientServiceV1 } from '@/persistence/redis.client.impl.v1'

import { IRandomGeneratorInjectionToken, IDateTimeInjectionToken } from '@/infrastructure/infrastructure.module';
import { RandomGeneratorV1 } from '@/infrastructure/random.generator.impl.v1';
import { DateTimeV1 } from '@/infrastructure/datetime.impl.v1';
import { SessionQueryResolver } from './resolvers/session.query.resolver';
import { QuizQueryResolver } from './resolvers/quiz.query.resolver';
import { RedisClient } from 'redis';
import { ScoreCalculatorV1 } from '@/infrastructure/score.calculator.impl.v1';


@Module({
  providers: [ 
    { provide: IQuizDaoInjectionToken, useClass: QuizDaoV1 },
    { provide: IQuizUsecaseInjectionToken, useClass: QuizUsecaseV1 },
    { provide: ISessionDaoInjectionToken, useClass: SessionDaoV1 },
    { provide: ISessionUsecaseInjectionToken, useClass: SessionUsecaseV1 },
    { provide: IQuestionDaoInjectionToken, useClass: QuestionDaoV1 },
    { provide: IChoiceDaoInjectionToken, useClass: ChoiceDaoV1 },
    { provide: IEventDaoInjectionToken, useClass: EventDaoV1 },
    { provide: IEventUsecaseInjectionToken, useClass: EventUsecaseV1 },
    { provide: IEventPubsubInjectionToken, useClass: EventPubsubV1 },
    { provide: IRandomGeneratorInjectionToken, useClass: RandomGeneratorV1 },
    { provide: IDateTimeInjectionToken, useClass: DateTimeV1 },
    { provide: IKeyValueDatabaseInjectionToken, useClass: RedisClientServiceV1},
    { provide: IScoreCalculatorInjectionToken, useClass: ScoreCalculatorV1 },
    QuestionResolver, 
    QuizResolver,
    QuizQueryResolver,
    QuizMutationResolver,
    SessionResolver,
    SessionMutationResolver,
    SessionSubscriptionResolver,
    SessionQueryResolver,
  ]
})
export class AppGraphQLModule {}