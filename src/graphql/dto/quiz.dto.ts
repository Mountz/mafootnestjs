import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class CreateQuizOutput {
  @Field(type => Int)
  id: number;

  @Field()
  title: string;
}