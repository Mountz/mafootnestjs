import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';
import { PlayerScore } from './player.dto';

@InputType()
export class QuestionUpdateInput {
  @Field()
  title?: string;

  @Field(type => Int)
  index: number;

  @Field(type => [ChoiceUpdateInput], { nullable: true })
  choices?: ChoiceUpdateInput[];

  @Field(type => [Int], { nullable: true })
  correctChoices?: number[];
}
@InputType()
export class ChoiceUpdateInput {
  
  @Field(type => Int)
  index: number;

  @Field({ nullable: true })
  content?: string;
}