import { Int, Field } from "@nestjs/graphql";

export class PlayerJoined {
  @Field()
  displayName: string;
}
export interface QuestionStarted {
  questionIndex: number;
}
export interface QuestionEnded { }

export class ScoreboardUpdate { }