import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PlayerScore } from './player.dto';

@ObjectType()
export class Scoreboard {
  @Field(type => [PlayerScore])
  scores: PlayerScore[];
}

