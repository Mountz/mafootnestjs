import { ObjectType, Field, Int } from "@nestjs/graphql";

@ObjectType()
export class PlayerScore {
  @Field()
  name: string;

  @Field(type => Int)
  score: number;
}