import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class CreateSessionOutput {
  @Field()
  sessionCode: string;

  @Field()
  title: string;

  @Field(type => Int)
  quizId: number;
}

@ObjectType()
export  class JoinSessionOutput {
  @Field(type => Int)
  sessionId: number;

  @Field(type => Int)
  playerId: number;

  @Field()
  displayName: string;

  @Field()
  token: string;
}