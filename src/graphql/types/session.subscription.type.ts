import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class SessionSubscription {
  @Field(type => Boolean)
  eventAdded: Promise<boolean>;
}