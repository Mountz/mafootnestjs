import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Quiz } from './quiz.type';
import { Player } from './player.type';
import { Score } from './score.type';
import { JoinSessionOutput } from '../dto/session.dto';
import { CreateQuizOutput } from '../dto/quiz.dto';

@ObjectType()
export class QuizMutation {
  @Field(type => CreateQuizOutput)
  create: Promise<CreateQuizOutput>;

  @Field(type => Boolean)
  updateTitle: Promise<boolean>

  @Field(type => Boolean)
  delete: Promise<boolean>

  @Field(type => Boolean)
  updateQuestion: Promise<boolean>

  @Field(type => Boolean)
  deleteQuestion: Promise<boolean>
}