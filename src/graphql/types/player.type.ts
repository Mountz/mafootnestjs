import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Player {
  @Field(type => Int)
  id: number;

  @Field()
  displayName: string;

  @Field()
  token: string;
}