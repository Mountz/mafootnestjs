import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Event } from './event.type';
import { Session } from './session.type';
import { PlayerScore } from '../dto/player.dto';
import { Player } from './player.type';

@ObjectType()
export class SessionQuery {
  @Field(type => Session, {nullable: true})
  get: Session;
  
  @Field(type => [ Event ])
  events: Promise<Event[]>;

  @Field(type => [ PlayerScore ])
  scoreboard: Promise<PlayerScore[]>

  @Field(type => [ Player ])
  players: Promise<Player[]>

  @Field(type => Int)
  playerCount: Promise<number>;
}