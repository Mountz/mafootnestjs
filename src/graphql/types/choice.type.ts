import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';

@ObjectType()
export class Choice {
  @Field(type => Int)
  id: number;

  @Field(type => Int)
  index: number;

  @Field()
  content: string;
}

@InputType()
export class ChoiceInput {
  @Field(type => Int)
  index: number;

  @Field()
  content: string;
}