import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Score {
  @Field(type => Int)
  id: number;

  @Field(type => Int)
  playerId: number;

  @Field(type => Int)
  score: number;
}