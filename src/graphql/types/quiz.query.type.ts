import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Quiz } from './quiz.type';

@ObjectType()
export class QuizQuery {
  @Field(type => Quiz, {nullable: true})
  get: Quiz;
  @Field(type => [ Quiz ])
  list: Promise<Quiz[]>;
}