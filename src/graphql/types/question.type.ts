import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';
import { Choice, ChoiceInput } from './choice.type';

@ObjectType()
export class Question {
  @Field(type => Int)
  id: number;

  @Field()
  title: string;

  @Field(type => Int)
  index: number;

  @Field(type => [ Choice ])
  choices: Choice[];

  @Field(type => [ Int ])
  correctChoices: number[];
}

@InputType()
export class QuestionInput {
  @Field()
  title: string;

  @Field(type => Int)
  index: number;

  @Field(type => [ ChoiceInput ])
  choices: ChoiceInput[];

  @Field(type => [ Int ])
  correctChoices: number[];
}