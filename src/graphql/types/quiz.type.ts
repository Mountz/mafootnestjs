import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';
import { Question, QuestionInput } from './question.type';

@ObjectType()
export class Quiz {
  @Field(type => Int)
  id: number;

  @Field()
  title: string;

  @Field(type => [ Question ])
  questions: Question[];
}

@InputType()
export class QuizInput {
  @Field()
  title: string;

  @Field(type => [ QuestionInput ])
  questions: QuestionInput[];
}