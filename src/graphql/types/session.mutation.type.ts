import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Quiz } from './quiz.type';
import { Player } from './player.type';
import { Score } from './score.type';
import { JoinSessionOutput, CreateSessionOutput } from '../dto/session.dto';

@ObjectType()
export class SessionMutation {

  @Field(type => CreateSessionOutput)
  create: Promise<CreateSessionOutput>

  @Field(type => Boolean)
  check: Promise<boolean>;

  @Field(type => JoinSessionOutput)
  join: Promise<JoinSessionOutput>;

  @Field(type => Int)
  submit: Promise<number>;

  @Field(type => Int)
  endQuestion: Promise<number>;

  @Field(type => Int)
  startQuestion: Promise<number>;
}