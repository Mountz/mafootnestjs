import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Quiz } from './quiz.type';
import { Player } from './player.type';
import { Score } from './score.type';
import { Event } from './event.type';

@ObjectType()
export class Session {
  @Field()
  title: string;
  
  @Field()
  code: string;

  @Field()
  quiz: Quiz;

  @Field(type => [ Player ])
  players: Player[];

  @Field(type => [ Score ])
  scores: Score[];
}