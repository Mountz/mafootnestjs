import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';

@ObjectType()
export class Event {
  @Field(type => Int)
  id: number;


  @Field(type => Int)
  sequence: number;

  @Field()
  type: "PlayerJoined" | "QuestionStarted" | "QuestionEnded";

  @Field()
  data: string;

  @Field(type => Int)
  date: number;
}