import { Resolver, Query, Args, Int, ResolveField, Parent, Mutation} from "@nestjs/graphql";
import { Inject } from "@nestjs/common";

import { Quiz, QuizInput } from "../types/quiz.type";
import { IQuizUsecase } from "@/application/interfaces/usecase/quiz.usecase.interface";
import { IQuizUsecaseInjectionToken } from "@/application/application.module";
import { QuestionUpdateInput } from "../dto/question.dto";
import { CreateQuizOutput } from "../dto/quiz.dto";
import { QuizMutation } from "../types/quiz.mutation.type";

@Resolver(of => QuizMutation)
export class QuizMutationResolver {
  
  constructor(
    @Inject(IQuizUsecaseInjectionToken) private readonly quizUsecase: IQuizUsecase,
  ){ }

  @Mutation(returns => QuizMutation)
  async quiz(): Promise<QuizMutation> {
    return new QuizMutation();
  }

  @ResolveField(returns => CreateQuizOutput)
  async create(@Args('q') q: QuizInput): Promise<CreateQuizOutput> {
    const quiz = {
      id: undefined,
      questions: q.questions,
      title: q.title,
    } as Quiz;
    const result = await this.quizUsecase.createQuiz(quiz);
    return {id: quiz.id, title: quiz.title} as CreateQuizOutput;
  }

  @ResolveField(returns => Boolean)
  async updateTitle(
    @Args('id', {type: () => Int}) id: number, 
    @Args('title') title: string
  ): Promise<boolean> {
    const result = await this.quizUsecase.updateQuizTitleById(id, title);
    return result;
  }

  @ResolveField(returns => Boolean)
  async deleteById(@Args('id', {type: () => Int}) id: number): Promise<boolean> {
    const result = await this.quizUsecase.deleteQuizById(id);
    return result;
  }

  @ResolveField(returns => Boolean)
  async updateQuestion(
    @Args('id', {type: () => Int}) id: number, 
    @Args('questions', {type: () => [QuestionUpdateInput]}) questions: QuestionUpdateInput[],
  ): Promise<boolean> {
    const result = await this.quizUsecase.updateQuestionById(id, questions);
    return result;
  }

  @ResolveField(returns => Boolean)
  async deleteQuestion(
    @Args('id', {type: () => Int}) id: number,
    @Args('index', {type: () => Int}) index: number,
  ): Promise<boolean> {
    const result = await this.quizUsecase.deleteQuestionByIndex(id, index);
    return result;
  }
}