import { Resolver, Query, Args, Int, ResolveField, Parent, Mutation } from "@nestjs/graphql";
import { Inject } from "@nestjs/common";

import { Score } from "../types/score.type";
import { Session } from "../types/session.type";

import { ISessionUsecaseInjectionToken } from "@/application/application.module";
import { ISessionUsecase } from "@/application/interfaces/usecase/session.usecase.interface";
import { CreateSessionOutput, JoinSessionOutput } from "../dto/session.dto";


@Resolver(of => Session)
export class SessionResolver {
  constructor(
    @Inject(ISessionUsecaseInjectionToken) private readonly sessionUsecase: ISessionUsecase
  ) { }
  
  // @Query(returns => Session, { nullable: true })
  // async session(@Args('id', { type: () => Int }) id: number): Promise<Session> {
  //   const result = this.sessionUsecase.getById(id);
  //   return result;
  // }

  @ResolveField(returns => [ Score ])
  async scores(
    @Parent() session: Session, 
    @Args('size',{ type: () => Int, defaultValue: 5 }) size: number
  ): Promise<Score[]> {
    const sessionId = await this.sessionUsecase.getIdByCode(session.code);
    const sc = await this.sessionUsecase.getScores(sessionId, size);
    return sc;
  }
}