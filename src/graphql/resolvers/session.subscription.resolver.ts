import { Resolver, Query, Args, Int, ResolveField, Parent, Mutation, Subscription} from "@nestjs/graphql";
import { Inject } from "@nestjs/common";
import { PubSub } from "graphql-subscriptions";

import { ISessionUsecaseInjectionToken, IEventPubsubInjectionToken, IEventDaoInjectionToken, IEventUsecaseInjectionToken } from "@/application/application.module";
import { ISessionUsecase } from "@/application/interfaces/usecase/session.usecase.interface";

import { SessionSubscription } from "../types/session.subscription.type";

import { Event } from "../types/event.type";
import { IEventPubSub } from "@/application/interfaces/pubsub/event.pubsub.interface";
import { EventAddedPubSubName } from "@/persistence/pubsub/pubsub.name";
import { IEventDao } from "@/application/interfaces/dao/event.dao.interface";
import { IEventUsecase } from "@/application/interfaces/usecase/event.usecase.interface";


@Resolver(of => SessionSubscription)
export class SessionSubscriptionResolver {
  constructor(
    @Inject(ISessionUsecaseInjectionToken) private readonly sessionUsecase: ISessionUsecase,
    @Inject(IEventPubsubInjectionToken) private readonly eventPubSub: IEventPubSub,
    @Inject(IEventUsecaseInjectionToken) private readonly eventUsecase: IEventUsecase,
  ) { }


  @Subscription(
    returns => [ Event ], { name: EventAddedPubSubName }
  )
  async eventHandler(
    @Args('code') code: string, 
  ): Promise<AsyncIterator<Event[]>> {
    const sessionId = await this.sessionUsecase.getIdByCode(code);
    if (sessionId) {
      return this.eventPubSub.subscription(sessionId);
    }
    return null;
  }
}