import { Resolver, Query, Args, Int, ResolveField, Parent, Mutation} from "@nestjs/graphql";
import { Inject } from "@nestjs/common";

import { Quiz } from "../types/quiz.type";
import { IQuizUsecase } from "@/application/interfaces/usecase/quiz.usecase.interface";
import { IQuizUsecaseInjectionToken } from "@/application/application.module";


@Resolver(of => Quiz)
export class QuizResolver {
  
  constructor(
    @Inject(IQuizUsecaseInjectionToken) private readonly quizUsecase: IQuizUsecase,
  ){ }

  @Query(returns => Quiz, { nullable: true })
  async quiz(@Args('id', { type: () => Int }) id: number): Promise<Quiz> {
    const result = await this.quizUsecase.getById(id);
    return result;
  }
}