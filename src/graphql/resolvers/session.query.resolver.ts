import { Resolver, Query, Args, Int, ResolveField, Parent, Mutation, Subscription} from "@nestjs/graphql";
import { Inject } from "@nestjs/common";
import { PubSub } from "graphql-subscriptions";

import { ISessionUsecaseInjectionToken, IEventPubsubInjectionToken, IEventDaoInjectionToken, IEventUsecaseInjectionToken } from "@/application/application.module";
import { ISessionUsecase } from "@/application/interfaces/usecase/session.usecase.interface";

import { SessionSubscription } from "../types/session.subscription.type";

import { Event } from "../types/event.type";
import { IEventPubSub } from "@/application/interfaces/pubsub/event.pubsub.interface";
import { EventAddedPubSubName } from "@/persistence/pubsub/pubsub.name";
import { IEventDao } from "@/application/interfaces/dao/event.dao.interface";
import { IEventUsecase } from "@/application/interfaces/usecase/event.usecase.interface";
import { SessionQuery } from "../types/session.query.type";
import { Session } from "../types/session.type";
import { Score } from "../types/score.type";
import { PlayerScore } from "../dto/player.dto";
import { Player } from "../types/player.type";


@Resolver(of => SessionQuery)
export class SessionQueryResolver {
  constructor(
    @Inject(ISessionUsecaseInjectionToken) private readonly sessionUsecase: ISessionUsecase,
    @Inject(IEventUsecaseInjectionToken) private readonly eventUsecase: IEventUsecase,
  ) { }


  @Query(returns => SessionQuery)
  async session() {
    return new SessionQuery;
  }

  @ResolveField(returns => [Event])
  async events(
    @Args('code') code: string,
    @Args('from', {type: () => Int}) from: number,
    @Args('to', {type: () => Int}) to: number,
  ): Promise<Event[]> {
    const sessionId = await this.sessionUsecase.getIdByCode(code);
    const result = await this.eventUsecase.getBySessionId(sessionId, from, to);
    return result;
  }

  @ResolveField(returns => Session, { nullable: true })
  async get(@Args('code') code: string): Promise<Session> {
    const result = this.sessionUsecase.getByCode(code);
    return result;
  }

  @ResolveField(returns => [PlayerScore])
  async scoreboard(
    @Args('code') code: string, 
    @Args('size', {type: () => Int, defaultValue: 5 }) size: number): Promise<PlayerScore[]> {
    const id = await this.sessionUsecase.getIdByCode(code);
    const playerList = await this.sessionUsecase.getPlayers(id, size);
    const result = await this.sessionUsecase.getScores(id, size);
    const sb = result.map(x => {
      const ps = new PlayerScore();
      ps.name = playerList.find(player => player.id == x.playerId).displayName;
      ps.score = x.score;
      return ps;
    })
    return sb;
  }

  @ResolveField(returns => [Player])
  async players(
    @Args('code') code: string,
    @Args('size', {type: () => Int}) size: number,
  ): Promise<Player[]> {
    const id = await this.sessionUsecase.getIdByCode(code);
    const players = await this.sessionUsecase.getPlayers(id, size);
    return players;
  }

  @ResolveField(returns => Int)
  async playerCount(
    @Args('code') code: string,
  ): Promise<number> {
    const id = await this.sessionUsecase.getIdByCode(code);
    const players = await this.sessionUsecase.getPlayers(id, 100);
    return players.length;
  }

  // @ResolveField(returns => [ Score ])
  // async scores(
  //   @Parent() session: Session, 
  //   @Args('size',{ type: () => Int, defaultValue: 5 }) size: number
  // ): Promise<Score[]> {
  //   const sc = await this.sessionUsecase.getScoreboard(session.id, size);
  //   return sc;
  // }

  
  
}