import { Resolver, Query, Args, Int, ResolveField, Parent, Mutation} from "@nestjs/graphql";
import { Inject } from "@nestjs/common";

import { Quiz } from "../types/quiz.type";
import { IQuizUsecase } from "@/application/interfaces/usecase/quiz.usecase.interface";
import { IQuizUsecaseInjectionToken } from "@/application/application.module";
import { QuizQuery } from "../types/quiz.query.type";

@Resolver(of => QuizQuery)
export class QuizQueryResolver {
  
  constructor(
    @Inject(IQuizUsecaseInjectionToken) private readonly quizUsecase: IQuizUsecase,
  ){ }
  @Query(returns => QuizQuery)
  async quiz(): Promise<QuizQuery> {
    return new QuizQuery();
  }

  @ResolveField(returns => [ Quiz ])
  async list(@Args('size', {type: () => Int}) size: number): Promise<Quiz[]> {
    const result = await this.quizUsecase.getMany(size);
    return result;
  }

  @ResolveField(returns => Quiz, { nullable: true })
  async get(@Args('id', { type: () => Int }) id: number): Promise<Quiz> {
    const result = await this.quizUsecase.getById(id);
    return result;
  }
}