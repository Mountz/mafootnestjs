import { Resolver, Query, Args, Int, ResolveField, Parent, Mutation, Context, GraphQLExecutionContext } from "@nestjs/graphql";
import { Inject } from "@nestjs/common";

import { ISessionUsecaseInjectionToken, IEventPubsubInjectionToken } from "@/application/application.module";
import { ISessionUsecase } from "@/application/interfaces/usecase/session.usecase.interface";
import { IEventPubSub } from "@/application/interfaces/pubsub/event.pubsub.interface";

import { SessionMutation } from "../types/session.mutation.type";
import { JoinSessionOutput, CreateSessionOutput } from "../dto/session.dto";


@Resolver(of => SessionMutation)
export class SessionMutationResolver {
  constructor(
    @Inject(ISessionUsecaseInjectionToken) private readonly sessionUsecase: ISessionUsecase,
  ) { }


  @Mutation(returns => SessionMutation)
  async session(): Promise<SessionMutation> {
    return new SessionMutation();
  }

  @ResolveField(returns => CreateSessionOutput, { nullable: true })
  async create(
    @Args('quizId', {type: () => Int}) quizId: number,
    @Args('title') title: string
  ): Promise<CreateSessionOutput> {
    const id = await this.sessionUsecase.createSession(quizId, title);
    const result = await this.sessionUsecase.getById(id)
    if (result) {
      return {
        sessionCode: result.code, 
        title: result.title,
        quizId: quizId
      };
    }
    return null;
  }

  @ResolveField(returns => Boolean)
  async join(
    @Args('code') code: string, 
    @Args('name') name: string,
  ): Promise<JoinSessionOutput> {
    const result = await this.sessionUsecase.joinSession(code, name);
    if (!result) {
      throw new Error("Can't join to the session")
    }
    return {
      displayName: name,
      token: result.token,
      playerId: result.id,
      sessionId: result.id,
    };
  }

  @ResolveField(returns => Boolean)
  async quit(  
    @Args('token', {type: () => Int}) token: string
  ): Promise<boolean> {
    const complete = await this.sessionUsecase.quitSession(token);
    if (complete) {
      return true;
    }
    return false;
  }

  @ResolveField(returns => Boolean)
  async check(
    @Args('code') code: string,
  ): Promise<boolean> {
    const result = await this.sessionUsecase.checkByCode(code)
    return result;
  }


  @ResolveField(returns => Int)
  async endQuestion(@Args('code') code: string,): Promise<number> {
    const result = await this.sessionUsecase.endQuestion(code);
    return 1;
  }

  @ResolveField(returns => Int)
  async startQuestion(
    @Args('code') code: string,
    @Args('index', {type: () => Int}) index: number): Promise<number> {
    const result = await this.sessionUsecase.startQuestion(code, index);
    return 1;
  }

  @ResolveField(returns => Int)
  async submit(
    @Args('token') token: string,
    @Args('answer', {type: () => Int}) answer: number,
  ): Promise<number> {
    const score = await this.sessionUsecase.submitAnswer(token, answer);
    return score;
  }
  
}