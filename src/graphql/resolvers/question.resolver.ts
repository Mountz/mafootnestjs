import { Resolver, Query, Args, Int, ResolveField, Parent } from "@nestjs/graphql";
import { Choice } from "../types/choice.type";
import { Question } from "../types/question.type";

@Resolver(of => Question)
export class QuestionResolver {
  
  @Query(returns => Question, { nullable: true })
  async question(@Args('id', { type: () => Int }) id: number): Promise<Question> {
    const q = new Question();
    q.id = id;
    q.title = "This is title";
    q.choices = [ new Choice() ];
    return q;
  }
}