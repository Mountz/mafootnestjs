import { Injectable, Inject } from "@nestjs/common";

import { EntityManager } from "typeorm";
import * as DM from "@/domain/entities";
import { ISessionDao } from "@/application/interfaces/dao/session.dao.interface";
import { IKeyValueDatabaseInjectionToken } from "@/application/application.module";
import { IKeyValueDatabase } from "@/application/interfaces/kv.database.interface";

import * as PM from "@persistence/models";
import * as ScoreMapper from "../mapper/score.mapper"
import { MapSessionPM2DM } from "../mapper/session.mapper";
import { MapPlayerPM2DM } from "../mapper/player.mapper";

@Injectable()
export class SessionDaoV1 implements ISessionDao {
  
  constructor(
    private readonly entityManager: EntityManager,
    @Inject(IKeyValueDatabaseInjectionToken) private readonly keyvalueDatabase: IKeyValueDatabase,
  ) { }

  async getById(id: number): Promise<DM.Session> {
    const result = await this.entityManager.findOne(PM.Session, id);
    if (result) {
      const session = MapSessionPM2DM(result);
      return session;
    }
    return null;
  }

  async getByCode(code: string): Promise<DM.Session> {
    const result = await this.entityManager.findOne(PM.Session, {code: code});
    if (result) {
      const session = MapSessionPM2DM(result);
      return session;
    }
    return null;
  }

  async getIdByCode(code: string): Promise<number> {
    const result = await this.entityManager.createQueryBuilder().
      select("S.id").
      from(PM.Session, "S").
      where(`"S"."code" = :code`, { code: code}).
      getOne();
    if (result) {
      return result.id;
    }
    return null;
  }
  
  async getScores(id: number, size: number): Promise<DM.Score[]> {
    const result = await this.entityManager.createQueryBuilder().
      select("S").
      from(PM.Score, "S").
      where(`"S"."sessionId" = :id`, { id: id }).
      orderBy(`"S"."score"`, "DESC").
      limit(size).
      getMany();
    const scoreboard = result.map(x => {
      return ScoreMapper.MapScorePM2DM(x);
    })
    return scoreboard;
  }

  async getPlayers(id: number, size: number): Promise<DM.Player[]> {
    const result = await this.entityManager.find(PM.Player, { where:{sessionId: id}, take: size})
    const players = result;
    return players;
  }

  async getPlayerByToken(token: string): Promise<DM.Player> {
    const player = await this.entityManager.findOne(PM.Player, {token: token});
    return player;
  }

  async getIdByPlayerToken(token: string): Promise<number> {
    const player = await this.entityManager.findOne(PM.Player, {token: token});
    return player.sessionId;
  }

  async createSession(quizId: number, code: string, title: string): Promise<number> {
    const quiz = await this.entityManager.findOne(PM.Quiz, {id: quizId});
    if (quiz) {
      const session = new PM.Session();
      session.id = 0;
      session.code = code;
      session.title = title;
      session.players = [] as PM.Player[];
      session.quiz = {id: quizId} as PM.Quiz;
      session.scores = [] as PM.Score[];
      const result = await this.entityManager.save(session);
      await this.keyvalueDatabase.set('session_'+session.id, "0");
      await this.keyvalueDatabase.expire('session_'+session.id, 7200);
      return result.id;
    }
    return null;
  }
  async addPlayerById(id: number, player: DM.Player): Promise<DM.Player> {
    const pmPlayer = new PM.Player()
    pmPlayer.id = undefined;
    pmPlayer.displayName = player.displayName;
    pmPlayer.token = player.token;
    pmPlayer.sessionId = id;
    
    const result = await this.entityManager.save(pmPlayer);
    if (result) {
      const pmScore = new PM.Score()
      pmScore.id = 0;
      pmScore.playerId = result.id;
      pmScore.sessionId = id;
      pmScore.score = 0;
      const sc = await this.entityManager.save(pmScore);
      return MapPlayerPM2DM(result);
    }
    return null;
  }
  async deletePlayerById(playerId: number): Promise<boolean> {
    const result = await this.entityManager.delete(PM.Player, {id: playerId});
    return result.affected > 0;
  }
  async deleteSessionById(id: number): Promise<boolean> {
    const result = await this.entityManager.delete(PM.Session, {id: id});
    return result.affected > 0;
  }
  async increasePlayerScore(id: number, score: number): Promise<boolean> {
    const pm = await this.entityManager.findOne(PM.Score, {playerId: id});
    pm.score += score;
    const result = await this.entityManager.save(pm);
    if (result) {
      return true;
    }
    return false;
  }
  saveChanges(): Promise<number> {
    throw new Error("Method not implemented.");
  }
}