import { Injectable } from "@nestjs/common";


import { EntityManager } from "typeorm";

import * as DM from "@/domain/entities";
import { QuestionUpdateDTO } from "@/application/interfaces/dao/quiz.dao.interface";
import { IQuestionDao } from "@/application/interfaces/dao/question.dao.interface";

import * as PM from "@persistence/models";
import { MapQuestionPM2DM } from "../mapper/question.mapper";


@Injectable()
export class QuestionDaoV1 implements IQuestionDao {
  
  constructor(
    private readonly entityManager: EntityManager,
  ) { }
  getByIndex(quizId: number, index: number): Promise<import("../../graphql/types/question.type").Question> {
    throw new Error("Method not implemented.");
  }
  async getByQuizId(id: number): Promise<import("../../graphql/types/question.type").Question[]> {
    const result = await this.entityManager.find(PM.Question, {quiz: id})
    if (result) return result.map(x => MapQuestionPM2DM(x));
    return null;
  }
  async removeById(id: number): Promise<number> {
    const result = await this.entityManager.delete(PM.Question, {id: id})
    return result.affected;
  }
  async updateByQuizId(id: number, question: QuestionUpdateDTO): Promise<boolean> {
    const old = await this.entityManager.find(PM.Question, {quiz: id});
    const index = old.findIndex(i => i.index == question.index);
    const qu = new PM.Question();
    if (index < 0) {
        qu.id = 0;
        qu.index = question.index;
        qu.title = question.title;
        qu.quiz = id;
    } else {
      qu.id = old[index].id;
      qu.index = question.index;
      qu.quiz = id;
      if (question.title) {
        qu.title = question.title;
      };
    }
    const result = await this.entityManager.save(qu);
    if (result) {
      return true;
    }
    return false;
  }
    
  saveChanges(): Promise<number> {
    throw new Error("Method not implemented.");
  }
  
}