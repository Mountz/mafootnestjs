import { Injectable } from "@nestjs/common";
import { EntityManager, Repository } from "typeorm";

import { Question } from "@/domain/entities/question.entity";
import * as DM from "@/domain/entities";
import { IQuizDao, QuestionUpdateDTO } from "@/application/interfaces/dao/quiz.dao.interface";

import * as PM from "@persistence/models";
import * as QuizMapper from "../mapper/quiz.mapper";
import * as QuestionMapper from "../mapper/question.mapper";

@Injectable()
export class QuizDaoV1 implements IQuizDao {

  constructor(
    private readonly entityManager: EntityManager,
  ) {  }

  async getById(id: number): Promise<DM.Quiz> {
    const result = await this.entityManager.findOne(PM.Quiz, id);
    if (result) {
      const quiz = QuizMapper.MapQuizPM2DM(result);
      return quiz
    }
    return null;
  }

  async getMany(size: number): Promise<DM.Quiz[]> {
    const result = await this.entityManager.find(PM.Quiz, {take: size});
    const dm = result.map(x => QuizMapper.MapQuizPM2DM(x));
    return dm;
  }

  async updateQuizTitleById(id: number, title: string): Promise<boolean> {
    const result = await this.entityManager.update(PM.Quiz, id, {title: title});
    return result.affected > 0;
  }
  async addQuestionById(id: number, questions: Question[]): Promise<number> {
    const temp = questions.map(x => {
      return QuestionMapper.MapQuestionDM2PM(x);
    });
    const result = await this.entityManager.save(temp);
    return result.length;
  }
  async updateQuestionById(id: number, question: QuestionUpdateDTO): Promise<boolean> {
    const result = await this.entityManager.findOne(PM.Quiz, id);
    if (!result) {
      return false;
    }
    const dm = QuizMapper.MapQuizPM2DM(result);
    const index = dm.questions.findIndex(x => x.index == question.index);
    if (index < 0) {
      return false;
    }
    if (question.title) {
      dm.questions[index].title = question.title;
    }
    const pm = QuizMapper.MapQuizDM2PM(dm);
    const r = await this.entityManager.save(pm);
    if (r) {
      return true;
    }
    return false;
  }
  async deleteQuizById(id: number): Promise<boolean> {
    const result = await this.entityManager.delete(PM.Quiz, {id: id});
    return result.affected > 0;
  }
  saveChanges(): Promise<number> {
    throw new Error("Method not implemented.");
  }
  
  async createQuiz(quiz: DM.Quiz): Promise<void> {
    const q = QuizMapper.MapQuizDM2PM(quiz);
    const result = await this.entityManager.save(q);
    quiz.id = result.id;
    return;
  }
  
}