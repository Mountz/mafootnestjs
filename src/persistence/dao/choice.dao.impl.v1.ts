import { Injectable } from "@nestjs/common";
import { EntityManager } from "typeorm";

import * as DM from "@/domain/entities";
import { ChoiceUpdateDTO } from "@/application/interfaces/dao/quiz.dao.interface";
import { IChoiceDao } from "@/application/interfaces/dao/choice.dao.interface";

import * as PM from "@persistence/models";

@Injectable()
export class ChoiceDaoV1 implements IChoiceDao {
  
  constructor(
    private readonly entityManager: EntityManager,
  ) { }

  async getByQuestionId(id: number): Promise<DM.Choice[]> {
    const result = await this.entityManager.find(PM.Choice, {question: id})
    if (result) return result;
    return null;
  }

  async updateChoiceByQuestionId(id: number, choices: ChoiceUpdateDTO[], correctList: number[]): Promise<number> {
    const old = await this.entityManager.find(PM.Choice, {question: id});
    const delResult = await this.entityManager.delete(PM.Choice, {question: id});
    const pmChoices = choices.map(x => {
      const index = old.findIndex(i => i.index == x.index);
      const ch = new PM.Choice();
      if (index < 0) {
          ch.id = 0;
          ch.index = x.index;
          ch.content = x.content;
          ch.question = id;
          ch.isCorrect = correctList.includes(x.index);
      } else {
        ch.id = old[index].id;
        ch.index = x.index;
        ch.question = id;
        if (x.content) {
          ch.content = x.content;
        };
        ch.isCorrect = correctList.includes(x.index);
      }
      return ch;
    });
    const result = await this.entityManager.save(pmChoices);
    return result.length;
  }

  async removeChoiceByQuestionId(id: number): Promise<number> {
    const result = await this.entityManager.delete(PM.Choice, {question: id})
    return result.affected;
  }
  saveChanges(): Promise<number> {
    throw new Error("Method not implemented.");
  }
}