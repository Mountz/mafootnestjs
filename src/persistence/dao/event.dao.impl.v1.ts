import { IEventDao } from "@/application/interfaces/dao/event.dao.interface";
import { Event } from "@/graphql/types/event.type";
import * as DM from "@/domain/entities";

import * as PM from "@persistence/models";
import { Inject } from "@nestjs/common";
import { IEventPubsubInjectionToken, IKeyValueDatabaseInjectionToken } from "@/application/application.module";
import { EntityManager } from "typeorm";
import { IDateTimeInjectionToken } from "@/infrastructure/infrastructure.module";
import { IDateTime } from "@/application/interfaces/datetime.interface";
import { IEventPubSub } from "@/application/interfaces/pubsub/event.pubsub.interface";
import { PlayerJoined, QuestionStarted } from "@/graphql/dto/event.dto";
import { IKeyValueDatabase } from "@/application/interfaces/kv.database.interface";

export class EventDaoV1 implements IEventDao {

  constructor(
    private readonly entityManager: EntityManager,
    @Inject(IDateTimeInjectionToken) private readonly dateTime: IDateTime,
    @Inject(IEventPubsubInjectionToken) private readonly eventPubsub: IEventPubSub,
    @Inject(IKeyValueDatabaseInjectionToken) private readonly keyvalueDatabase: IKeyValueDatabase,
  ) { }
  
  async getBySessionId(sessionId: number): Promise<DM.Event[]> {
    const result = await this.entityManager.find(PM.Event, {session: sessionId});
    return result;
  }

  async getLatestQuestionEvent(sessionId: number): Promise<Event> {
    const result = await this.entityManager.find(
      PM.Event, {
        where: {
          session: sessionId, 
          type: "QuestionStarted"
        }, 
        order: { date: "DESC"}
      }
    );
    if (result) {
      return result[0];
    }
    return null;
  }

  async addBySessionId(sessionId: number, event: Event): Promise<number> {
    const e = new PM.Event();
    e.id = undefined;
    e.sequence = await this.keyvalueDatabase.increment('session_'+sessionId);
    e.session = sessionId;
    e.type = event.type;
    e.data = event.data;
    e.date = event.date;
    const result = await this.entityManager.save(e);
    if (result) {
      return result.id;
    } else {
      return null;
    }
  }

  async addPlayerEvent(sessionId: number, playerName: string): Promise<number> {
    const e = new PM.Event();
    e.id = undefined;
    e.sequence = await this.keyvalueDatabase.increment('session_'+sessionId);
    e.session = sessionId;
    e.type = "PlayerJoined";
    e.data = JSON.stringify({displayName:playerName} as PlayerJoined);
    e.date = this.dateTime.getUnixSeconds();
    const result = await this.entityManager.save(e);
    if (result) {
      await this.eventPubsub.publish(sessionId, [result]);
      return result.id;
    } else {
      return null;
    }
  }

  async startQuestion(sessionId: number, index: number): Promise<number> {
    const e = new PM.Event();
    e.id = undefined;
    e.sequence = await this.keyvalueDatabase.increment('session_'+sessionId);
    e.session = sessionId;
    e.type = "QuestionStarted";
    e.data = JSON.stringify({questionIndex: index} as QuestionStarted);
    e.date = this.dateTime.getUnixSeconds();
    const result = await this.entityManager.save(e);
    if (result) {
      await this.eventPubsub.publish(sessionId, [result]);
      return result.id;
    } else {
      return null;
    }
  }

  async endQuestion(sessionId: number): Promise<number> {
    const e = new PM.Event();
    e.id = undefined;
    e.sequence = await this.keyvalueDatabase.increment('session_'+sessionId);
    e.session = sessionId;
    e.type = "QuestionEnded";
    e.data = JSON.stringify({});
    e.date = this.dateTime.getUnixSeconds();
    const result = await this.entityManager.save(e);
    if (result) {
      await this.eventPubsub.publish(sessionId, [result]);
      return result.id;
    } else {
      return null;
    }
  }

  saveChanges(): Promise<number> {
    throw new Error("Method not implemented.");
  }
  
}