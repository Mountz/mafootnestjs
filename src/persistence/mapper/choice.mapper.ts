import * as PM from '@persistence/models'
import * as DM from '@domain/entities'

export function MapChoicePM2DM(pm: PM.Choice): DM.Choice {
  const dm = {
    id: pm.id,
    index: pm.index,
    content: pm.content    
  } as DM.Choice
  return dm;
}

export function MapChoiceDM2PM(dm: DM.Choice, isCorrect: boolean = false, questionId: number = 0): PM.Choice {
  const pm = new PM.Choice();
  pm.id = dm.id > 0 ? dm.id: undefined;
  pm.index = dm.index;
  pm.isCorrect = isCorrect;
  pm.content = dm.content;
  return pm;
}