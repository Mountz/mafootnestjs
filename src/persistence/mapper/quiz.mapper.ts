import * as PM from '@persistence/models'
import * as DM from '@domain/entities'
import { MapQuestionPM2DM, MapQuestionDM2PM } from './question.mapper';

export function MapQuizPM2DM(pm: PM.Quiz): DM.Quiz {
  const dm = {
    id: pm.id,
    title: pm.title,
    questions: []
  } as DM.Quiz;
  dm.questions = pm.questions.map(x => {
    return MapQuestionPM2DM(x);
  })
  return dm;
}

export function MapQuizDM2PM(dm: DM.Quiz): PM.Quiz {
  const pm = new PM.Quiz();
  pm.id = dm.id > 0 ? dm.id: undefined;
  pm.title = dm.title;
  pm.questions = dm.questions.map(x => {
    return MapQuestionDM2PM(x);
  })
  return pm;
}