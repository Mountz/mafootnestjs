import * as PM from '@persistence/models'
import * as DM from '@domain/entities'

export function MapScorePM2DM(pm: PM.Score): DM.Score {
  const dm : DM.Score = {
    id: pm.id,
    playerId: pm.playerId,
    score: pm.score
  };
  return dm;
}