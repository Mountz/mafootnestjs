import * as PM from '@persistence/models'
import * as DM from '@domain/entities'

export function MapPlayerPM2DM(pm: PM.Player): DM.Player {
  const dm : DM.Player = {
    id: pm.id,
    displayName: pm.displayName,
    token: pm.token
  }
  return dm;
}