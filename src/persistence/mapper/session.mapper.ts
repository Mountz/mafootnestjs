import * as PM from '@persistence/models'
import * as DM from '@domain/entities'
import { MapQuizPM2DM } from './quiz.mapper'
import { MapPlayerPM2DM } from './player.mapper'
import { MapScorePM2DM } from './score.mapper'

export function MapSessionPM2DM(pm: PM.Session): DM.Session {
  const dm : DM.Session = {
    id: pm.id,
    code: pm.code,
    title: pm.title,
    quiz: MapQuizPM2DM(pm.quiz),
    players: pm.players.map(x => {
      return MapPlayerPM2DM(x);
    }),
    scores: pm.scores.map(x => {
      return MapScorePM2DM(x);
    })
  }
  return dm;
}