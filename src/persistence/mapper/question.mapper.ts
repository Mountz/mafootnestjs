import * as PM from '@persistence/models'
import * as DM from '@domain/entities'
import { MapChoiceDM2PM, MapChoicePM2DM } from './choice.mapper';

export function MapQuestionPM2DM(pm: PM.Question): DM.Question {
  const dm = {
    id: pm.id,
    title: pm.title,
    index: pm.index,
    choices: [],
    correctChoices: []
  } as DM.Question;
  dm.choices = pm.choices.map(y => {
    return MapChoicePM2DM(y);
  })
  dm.correctChoices = pm.choices.filter(y => y.isCorrect).map(y => y.index);
  return dm;
}

export function MapQuestionDM2PM(dm: DM.Question): PM.Question {
  const pm = new PM.Question();
  pm.id = dm.id > 0 ? dm.id: undefined;
  pm.index = dm.index;
  pm.title = dm.title;
  pm.choices = dm.choices.map(y => {
    return MapChoiceDM2PM(y, dm.correctChoices.includes(y.index),pm.id);
  })
  return pm;
}