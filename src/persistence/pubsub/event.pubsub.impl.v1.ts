import { IEventPubSub } from "@/application/interfaces/pubsub/event.pubsub.interface";
import { Event } from "@/domain/entities";
import { PubSub } from "graphql-subscriptions";
import { EventAddedPubSubName } from "./pubsub.name";

export class EventPubsubV1 implements IEventPubSub {

  constructor(
    private readonly pubSub: PubSub,
  ) {
    this.pubSub = new PubSub();
  }
  async publish(sessionId: number, event: Event[]): Promise<number> {
    const triggerName = EventAddedPubSubName+sessionId;
    this.pubSub.publish(triggerName, {eventAdded: event});

    return ;
  }
  async subscription(sessionId: number): Promise<AsyncIterator<Event[]>> {
    // this.pubSub.subscribe(EventAddedPubSubName+sessionId,)
    const triggerName = EventAddedPubSubName+sessionId;
    return this.pubSub.asyncIterator(triggerName);
  }
  
}