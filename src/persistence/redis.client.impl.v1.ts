import { IKeyValueDatabase } from "@/application/interfaces/kv.database.interface";
import { RedisService } from "nestjs-redis";
import * as Redis from 'ioredis';
import { Injectable } from "@nestjs/common";

@Injectable()
export class RedisClientServiceV1 implements IKeyValueDatabase {
  private readonly redisClient: Redis.Redis;
  constructor(
    private readonly redisService: RedisService,
  ) { 
    this.redisClient = this.redisService.getClient();
  }

  async get(key: string): Promise<string> {
    const result = await this.redisClient.get(key);
    return result;
  }
  async set(key: string, value: string): Promise<boolean> {
    const result = await this.redisClient.set(key, value);
    return result == 'OK';
  }
  async increment(key: string): Promise<number> {
    const result = await this.redisClient.incr(key);
    return result;
  }
  async expire(key: string, seconds: number): Promise<number> {
    const result = await this.redisClient.expire(key, seconds);
    return result;
  }
}