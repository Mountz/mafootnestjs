import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Session } from "./session.model";

@Entity("Event")
export class Event {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    sequence: number;

    @Column()
    type: "PlayerJoined" | "QuestionStarted" | "QuestionEnded";

    @Column()
    data: string;

    @Column()
    date: number;

    @ManyToOne(type => Session, { onDelete:"CASCADE"})
    session: number;
}