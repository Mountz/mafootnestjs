import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne} from "typeorm";
import { Choice } from "./choice.model"
import { Quiz } from "./quiz.model";

@Entity("Question")
export class Question {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    index: number;

    @Column()
    title: string;

    @ManyToOne(type => Quiz, quiz => quiz.questions, {onDelete:"CASCADE"})
    quiz: number;

    @OneToMany(type => Choice, choice => choice.question, {cascade: true, eager: true})
    choices: Choice[];
}