import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from "typeorm";
import { Session } from "./session.model";

@Entity("Player")
export class Player {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    displayName: string;

    @Column()
    token: string;

    @Column()
    sessionId: number;

    @ManyToOne(type => Session, s => s.players, { onDelete:"CASCADE" })
    session: Session;
}