import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from "typeorm";
import { Question } from "./question.model";

@Entity("Choice")
export class Choice {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Question, q => q.choices, {onDelete:"CASCADE"})
    question: number;

    @Column()
    content: string;

    @Column()
    index: number;

    @Column()
    isCorrect: boolean;
}