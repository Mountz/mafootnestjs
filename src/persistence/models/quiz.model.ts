import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from "typeorm";
import { Question } from "./question.model";

@Entity("Quiz")
export class Quiz {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @OneToMany(type => Question, q => q.quiz, {cascade: true, eager: true})
    questions: Question[];
}