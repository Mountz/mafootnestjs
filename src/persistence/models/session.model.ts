import {Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn, ManyToOne} from "typeorm";
import { Player } from "./player.model"
import { Score } from "./score.model";
import { Quiz } from "./quiz.model";

@Entity("Session")
export class Session {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    title: string;

    @ManyToOne(type => Quiz, {eager: true})
    quiz: Quiz;

    @OneToMany(type => Player, p => p.session, {eager: true})
    players: Player[];

    @OneToMany(type => Score, s => s.session, {eager: true})
    scores: Score[];
}