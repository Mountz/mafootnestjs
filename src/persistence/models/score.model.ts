import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, OneToOne, JoinTable, JoinColumn} from "typeorm";
import { Player } from "./player.model"
import { Session } from "./session.model";

@Entity("Score")
export class Score {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    sessionId: number;

    @ManyToOne(type => Session, s => s.scores, {onDelete:"CASCADE"})
    session: Session;

    @Column()
    playerId: number;

    @OneToOne(type => Player, {onDelete:"CASCADE"})
    @JoinColumn()
    player: Player;

    @Column()
    score: number;
}