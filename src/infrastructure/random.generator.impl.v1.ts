import { IRandomGenerator } from "@/application/interfaces/random.generator.interface";

export class RandomGeneratorV1 implements IRandomGenerator {
  getRandomString(length: number, allowNumber: boolean): string {
    let generated = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (allowNumber) {
      characters += '0123456789';
    }
    for (let i = 0; i < length; i++) {
      generated += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return generated
  }
  
  getRandomNumber(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) + min);
  }
}