import { IScoreCalculator } from "../application/interfaces/score.calculator.interface"

export class ScoreCalculatorV1 implements IScoreCalculator {
  calculateScore(maxScore: number, interval: number, maxTime: number): number {
    if (interval < 0) {
      return 0;
    }
    if (interval > maxTime) {
      return 0;
    }
    return Math.floor(((maxTime - interval) / maxTime) * maxScore);
  }
}