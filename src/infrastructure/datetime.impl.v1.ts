import { IDateTime } from "@/application/interfaces/datetime.interface";

export class DateTimeV1 implements IDateTime {
    getUnixMilliSeconds(): number {
        return new Date().getTime();
    }
    getUnixSeconds(): number {
        return Math.floor(new Date().getTime()/1000);
    }
}