import { Module } from '@nestjs/common';
import { IDateTime } from '@application/interfaces/datetime.interface';
import { DateTimeV1 } from './datetime.impl.v1';
import { RandomGeneratorV1 } from './random.generator.impl.v1';

export const IDateTimeInjectionToken = 'Infrastructure_IDateTime';
export const IRandomGeneratorInjectionToken = 'Infrastructure_IRandomGenerator';

@Module({
  providers: [
    { provide: IDateTimeInjectionToken, useClass: DateTimeV1 },
    { provide: IRandomGeneratorInjectionToken, useClass: RandomGeneratorV1}
  ],
  exports: [
    { provide: IDateTimeInjectionToken, useClass: DateTimeV1 },
    { provide: IRandomGeneratorInjectionToken, useClass: RandomGeneratorV1}
  ]
})
export class AppInfrastructureModule {}